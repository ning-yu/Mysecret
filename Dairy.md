# My secret

### 此文档已记录我每天的努力，用来提醒自己又离目标近了一步，每天能做的事情虽小，但对于我而言每件事都是属于我个人的event.

##### 

#### 2023年9月16日

​        今天制定了大目标：1.跑下半马。2.用Java并发实现业务功能。3.用Python实现一些业务功能。

​        英语方面也应该制定一个目标，现在还没有确切的目标，这个目标取决于我想今后用英语做什么。是应该做雅思练习题呢，还是应该花时间在英文的技术书上呢？既然尚未确定就暂且搁置。

​        今天由向我的大目标迈进了一小步：1.跑了速度跑，以5K pace的速度跑了8个间歇组。耗时19:20完成了3.13km。

2.开始看Thinking in Java书中关于并发的内容，对于翻译的不理解的部分，直接看的英文，没有取得Measurable的成果。

​        明天做事情要提醒自己是否是按照SMART的原则做事，S-specific,M-measurable,A-attainable,R-relevant,T-time based.例如看书要解决什么疑惑，要看几页书，是否真正理解内容，用多少时间去做这件事情，重要的事情是否留出了固定的时间。

​        明天记录事件的格式：在时间(   )，完成了(    )，距离（）的目标又近了一步。

​        明天要做的事情：1. Recovery Run 10min-2km.(注意练习舒畅的呼吸，四步一呼，四步一吸。)

​                                        2.过Java并发知识（学的阶段-需要多次重复）

​                                        3.过Python官方文档。

​        需要注意的事情：学的阶段--尽快开始整理归纳总结，自己动手去做。

​                                                        第一遍囫囵吞枣之后就开始总结、归纳、整理、组织关键知识点，用各种列表、示意图、表格，别怕麻烦。

​                                                        --读不懂也要读完，然后重复很多遍 -->就算不明白也要先记住。

​                                                        --看第二遍第三遍的时候就需要只字不差的阅读了。

​                                        刻意思考--这东西能用在哪呢？这东西还能用在哪呢？

​                                                      --刻意思考自己应该刻意练习哪些东西

​                                        追求全面--学的时候多看几本书

​                                        

#### 2024/9/16

1.绿皮书《Introducation to ML with Python - A Guide for Data Scientists》

    P256，notebook[35],

- cross_val_score()中cv=5与cv=KFold(n_splits=5)有啥区别？

        函数文档：For `int`/`None` inputs, if the estimator is a classifier and `y` is either binary or multiclass, [`StratifiedKFold`](https://scikit-learn.org/dev/modules/generated/sklearn.model_selection.StratifiedKFold.html#sklearn.model_selection.StratifiedKFold "sklearn.model_selection.StratifiedKFold") is used. In all other cases, [`KFold`](https://scikit-learn.org/dev/modules/generated/sklearn.model_selection.KFold.html#sklearn.model_selection.KFold "sklearn.model_selection.KFold") is used. These splitters are instantiated with `shuffle=False` so the splits will be the same across calls.

(https://scikit-learn.org/dev/modules/generated/sklearn.model_selection.cross_val_score.html)

BUT notebook[35] ,the estimator is regression ,it should have used KFold there.But the result of cv=5 is same with StratifedKFold(n_splits=5). **WHY?**

---

Answer: the LogisticRegression() is a classifier and not act like its name"Regression".

    So how to verify the type of estimator?

```python
#how to verify whether a estimator is classifier or regressor?
#come up by the cross_val_socre(logreg,cv=5) has same result with cv=StratifiedKFold(n_splits=5)

from sklearn.base import is_classifier,is_regressor
#m1
print(type(logreg))

#m2
print(is_classifier(logreg))
print(is_regressor(logreg))
#m3
#Check avaliable methods of the logisticRegression()
print(dir(logreg))
#Look for classifier-specific methods
print('predict_proba' in dir(logreg))
#Look for regressor-specific methods
print('predict' in dir(logreg))

#m4
#get documentation directly in the code environment.
#help(LogisticRegression)
```

#### 2024/9/17

1.    Cross-Validation - statistical method of evaluating generalization performance that is more stable and thorough than using a split into a trainning and a test set.

Benifit - 

accurate(exclude the situation of no test class was trained in the training set)/

range(the range of accuracies )/

effective(more data can get trained)/

2.Summary: use cross_val_score(*estimator*, *X*, *y=None*, ***, *groups=None*, *scoring=None*, *cv=None*, *n_jobs=None*, *verbose=0*, *fit_params=None*, *params=None*, *pre_dispatch='2*n_jobs'*, *error_score=nan*) from sklearn.model_selection,

before that we need to split the datasets through

 functions:ShuffleSplit(),StratifiedShuffleSplit(),

 attributes: 

cv=KFold(n_splits=(int)),cv=StratifiedKFold(n_splits=(int)),cv=(int),cv=LeaveOneOut( ),

KFold(shuffle=True),

3.Question :

first group y = [1,0,2] which means the generalization possibility is 1. 

second group y=[0,0,1,1] which means the score is  4/8(there are four 0 and 1 in test set) ,that's0.5

third group=[2,0] which the score is  6/10 ,that's 0.6

foutth group=[2,2,1] which the score is  5/9 that's 0.56 

So why the result score was [0.75 0.66666667 0.66666667 1.]?

```python
from sklearn.model_selection import GroupKFold
from sklearn.datasets import make_blobs
#create syntheic dataset
X,y = make_blobs(n_samples=12,random_state=0)
#assume the first three samples belong to the same group,
#then the next four,etc.
groups = [0,0,0,1,1,1,1,2,2,3,3,3]
scores = cross_val_score(logreg,X,y,groups=groups,cv=GroupKFold(n_splits=4))
print("Cross-validation scores:\n{}".format(scores))
print(X)
print(y)
```

```python
Cross-validation scores:
[0.75       0.66666667 0.66666667 1.        ]
[[ 3.54934659  0.6925054 ]
 [ 1.9263585   4.15243012]
 [ 0.0058752   4.38724103]
 [ 1.12031365  5.75806083]
 [ 1.7373078   4.42546234]
 [ 2.36833522  0.04356792]
 [-0.49772229  1.55128226]
 [-1.4811455   2.73069841]
 [ 0.87305123  4.71438583]
 [-0.66246781  2.17571724]
 [ 0.74285061  1.46351659]
 [ 2.49913075  1.23133799]]
[1 0 2 0 0 1 1 2 0 2 2 1]
```

#### 2024/9/18

1.Summary of Grid Search

def:imporve the model's generalization performance by tuning its parameters.

Question    :    **Why it was named "Grid Search"**

ANSWER:    This **grid of parameters is defined before the optimization/search step**, hence the name grid search.

2.impletation 

Question    : **Why only simple grid apply the "train,valid,test" set ?**

ANSWER    :IT'S NOT ONLY SIMPLE GRID APPLY THE VALIDATION SET.

In practice ,I just use the stratey in the simple grid case.

 train the model and choose best parameters on the valiadation set and then use the model score test set is moer accurate and **avoid Overfitting the parameters**

you can't just use test set to choose the parameters and think that is enough beacuse this accuracy won't necessarily carry over to new data.

- Simple Grid

```python
from sklearn.svm import SVC
#split data into train+validation set and test set 
X_trainval,X_test,y_trainval,y_test = train_test_split(iris.data,iris.target,random_state=0)
#split train+validation set into training and validation sets
X_train,X_valid,y_train,y_valid = train_test_split(X_trainval,y_trainval,random_state=1)
print("Size of training set :{} size of validation set :{} size of test set:{}\n".format(X_train.shape[0],X_valid.shape[0],X_test.shape[0]))
best_score = 0
for gamma in [0.001,0.01,0.1,1,10,100]:
    for C in [0.001,0.01,0.1,1,10,100]:
        #for each combination of parameters,train an SVC
        svm = SVC(gamma=gamma,C=C)
        svm.fit(X_train,y_train)
        #evaluate the SVC on the test set
        score = svm.score(X_valid,y_valid)
        #if we got a better score,store the score and parameters
        if score > best_score:
            best_score = score
            best_parameters = {'C':C, 'gamma':gamma}
        #rebulid a model on the combined training and validation set
        #and evalute it on the test set
svm = SVC(**best_parameters)
svm.fit(X_trainval,y_trainval)
test_score = svm.score(X_test,y_test)
print("Best score on validation set: {:.2f}".format(best_score))
print("Best parameters: " ,best_parameters)
print("Test set score with best parameters:{:.2f}".format(test_score))
```

- Grid Search with Cross-validation

```python
from sklearn.base import is_classifier
from sklearn.model_selection import cross_val_score
import numpy as np
print(is_classifier(SVC))
best_score = 0
for gamma in [0.001,0.01,0.1,1,10,100]:
    for C in [0.001,0.01,0.1,1,10,100]:
        #for each combination of parameters,
        #train an SVC
        svm = SVC(gamma=gamma,C=C)
        #perform cross-validation
        scores = cross_val_score(svm,X_trainval,y_trainval,cv=5)
        #compute mean cross-validation accuracy
        score = np.mean(scores)
        #if we got a better score,store the score and parameters
        if score > best_score:
            best_score = score
            best_parameters = {'C':C, 'gamma':gamma}
# rebuild a model on the combined training and validation set
svm = SVC(**best_parameters)
svm.fit(X_trainval,y_trainval)
print("best parameters:",best_parameters)
print("best socre: ",best_score)
```

```python
from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVC
#For integer/None inputs, if the estimator is a classifier and y is either binary or multiclass, StratifiedKFold is used. In all other cases, KFold is used. 
#above is from GridSearchCV documentation.
grid_search = GridSearchCV(SVC(),param_grid,cv=5)
X_train,X_test,y_train,y_test = train_test_split(iris.data,iris.target,random_state=0)
grid_search.fit(X_train,y_train)
print("")
print("Test set score: {:.2f}".format(grid_search.score(X_test,y_test)))
print("Best parameters: {}".format(grid_search.best_params_))
print("Best cross-validation score:{:.2f}".format(grid_search.best_score_))
print("Best estimator:\n{}".format(grid_search.best_estimator_))
```

#### 2024/9/19

Nested cross-validation

```python
scores = cross_val_score(GridSearchCV(SVC(),param_grid,cv=5),
                        iris.data,iris.target,cv=5)
```

StratifiedKFold 5 folds ( generally it 4 folds train set and one test set)

give the 4 folds to GridSearchCV which indicates  36 * 5 times building models to get best parameters.

the outer loop give inner loop 5 times data so totally 36 * 5  * 5 modes were built.

TOTALLY  36 * 5 * 5 + 5 models were trained by the above code.(the scores is a list inclueding 5 items,which is the last 5 times sum to the formula.) 

#### 2024/10/11

###### Nvidia CEO 黄仁勋在加州理工发表演讲：

2024 届的毕业生们，我几乎无法想象有谁比你们更为未来做好了准备。你们全身心投入，努力工作，从世界上最负盛名的学校之一获得了世界一流的教育。当你们进入下一个阶段时，请记住我的经验，希望它们能在你们前行的路上有所帮助。我希望你们相信某些东西，某些非传统的，某些未被探索的东西，但要让它有理有据。然后全身心投入让它实现。你可能会找到你的 GPU，你可能会找到你的 CUDA，你可能会找到你的生成式 AI，你可能会找到你的 NVIDIA。我希望你们能将挫折视为新的机会。你的痛苦和磨难会加强你的性格、韧性和灵活性，这些是终极的超级力量。

在我最看重的能力中，智慧并不是排名第一。我能忍受痛苦和磨难的能力，我能长期致力于某件事的能力，我能应对挫折并看到转角处机会的能力，我认为是我的超级力量，我希望它们也是你的超级力量。我希望你找到一门手艺。重要的不是在第一天就决定，甚至也不需要很快决定，但我希望你找到一门手艺，你愿意终身致力于完美它，磨练它的技能，让它成为你一生的工作。

最后，优先安排你的生活。有这么多事情要做，但优先安排你的生活，你会有充足的时间去做重要的事情。祝贺你们，2024 届毕业生，加油干。

SUMMARY:对于加州理工学生——发现探索并为之实现而努力积累

1.发现并实现很重要。

2.努力积累，成为一生的奋斗目标。

3.优先安排时间去做重要的事情。

###### 在台湾大学演讲：（对于台湾大学学生——用正确思维学习“掌握AI”并努力进步）

1.AI会创造过去不存在的新工作，每个人都要学习掌握AI红利。

2.Naidia故事后的思维：面对错误，勇于求助。为了实现愿景必然要忍受痛苦（为了学到东西必然要积累并刻意练习）、有策略的撤退，决定不去做什么。为了人生的使命和志业，你得做出牺牲并全心投入。

3.人是要努力的。

> “跑起来掠食，或是努力奔跑免得成为掠食者的食物。”

大多时候，你很难区分这两者。不管怎样，跑起来吧！
