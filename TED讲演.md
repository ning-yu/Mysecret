# TED讲演

2024/2/12

##### 没有方向？先从当个积极的迷惘人开始！    |王佑哲 Engene Wang | TEDxNTUST[https://www.youtube.com/watch?v=4FJEbzPhqq8&t=1s]

###### 想像這是一片充滿霧氣的湖，你坐在湖裡的某一艘船上，向四周望去都看不到岸。相信這是每個人心中偶爾會浮起的畫面；這座湖就是人生，而你是人生中的掌舵手。我們需要確信某個信念往某個方向划去，穿過孤獨而杳無人煙的迷霧才能到岸。

###### 佑哲將會以"積極的迷惘人"為出發點，指引我們該如何處理對於現狀不滿，而卻不知從何下手的情況。如何才能夠確立自己的目標而不失熱情？又怎麼知道自己前進的方向是自己想要的？這些問題的答案或許就在這套系統中。 王佑哲為亞洲最大群眾募資顧問公司貝殼放大的共同創辦人之一，之前更曾參與inside及flyingV等新創公司的創立。在持續累積自己的過程中，發展出獨特的"積極的迷惘人"這套系統，並以上百場的演說幫助到無數同樣在人生路口上的迷惘人。

- 积极的常识并不能解决迷惘

- 需要大量持续有功用的积累
  
  - 斜坡方法
    
            - 分享：找伙伴，得反馈
                - 碎片化：把一整件事当作一个系统，把系统碎片化，像程序一样自动完成相应动作，减少心理成本（畏难，懒惰）的损耗。

- 要产生价值
  将积累下来的东西视为一个工具，思考的点应该放在累积完节点后怎样产生大量的链接。把自己不同的工具与别人不同的工具结合在一起创造价值。

---------------------

##### 为什么不该追求爱情 Why shouldn't we "pursue" love? |国洋 张|TEDxFJU

###### 戀愛與職場原來有這麼多相似之處？商業顧問張國洋認為，不少人在談戀愛時常被中文所謂的「追求」二字給誤導了，傾向聽從直覺採取一個容易把對方嚇退的策略。 但實際上要發展長遠的戀愛關係，我們應像知名品牌建立客戶興趣那樣，採取一種釣魚型的策略：一面拿出價值與口碑吸引魚群；另一方面，則要避免急躁收線以免把魚線弄斷。

###### It is very similar between pursuing lover and building career. Business consultant JHANG, GUO YANG thinks a lot of people have misunderstood the meaning of "pursue the love", and often scare off the one. In fact, if people want to develop a long term relationship, should make the target interested in them just like to make the customer interested in the brand. Take fishing for example, fisher lure the fish with value and good reputation but doesn't rush to reel the thread in, otherwise, the string might break and the fish will go away.

<img src="https://gitee.com/ning-yu/Mysecret/raw/main/pic/对方抗拒.png" alt="演讲笔记" style="zoom:50%;" />